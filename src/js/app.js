import '../scss/index.scss'
import Vue from 'vue'
import router from './router/index'
import store from './store/index.js'
import App from './components/App.vue'
import vImg from 'v-img'

Vue.use(vImg)

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
