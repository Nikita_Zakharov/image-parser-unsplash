import api from '../utils/api'

export default {
    getImages(store, pageNumber) {
        return api.get('photos', {
            headers: {
                'X-Per-Page': 3
            },
            params: {
                client_id: 'f0c4a0b35f8df9c4c247570da900b992a72986e5730ec4f8f3ad2201638f06ee',
                page: pageNumber
            }
        })
            .then(res => {
                store.commit('SET_IMAGES', res.data)
            })
            .catch(err => {
                console.log(err)
            })
    }
}