import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/pages/Home.vue'
import Images from '../components/pages/Images.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {
            path: '/',
            component: Home,
            name: 'home',
            meta: {
                pageTitle: 'Home'
            }
        },
        {
            path: '/images/',
            redirect: '/images/1'
        },
        {
            path: '/images/:pageNumber',
            component: Images,
            name: 'images',
            meta: {
                pageTitle: 'Images from Unsplash'
            }
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0}
    }
})